# integrate a time history from the model runs

import numpy as np
import glob, os

runpath = '/home/tinu/projects/flowmodel/flow_glen/runs/sc11'
flowlinepath = '/home/tinu/projects/flowline'
flowlinecmd  = '/home/tinu/projects/flowline/calcvelo-opt'

# integration steps per year
nsteps = 0
# vertical points
npoints = 11
points = np.vstack((np.zeros(npoints), np.arange(npoints)*50.)).T

# save the outpoints for velocity interpolation
header = (runpath+'/out_02010 \n' + 
          '%d  %d  1' % (len(points), nsteps))
np.savetxt(flowlinepath+'/points.dat', points, header=header, comments='')

# calculate all velocities
os.system('cd %s; ' %(flowlinepath) + flowlinecmd)

newpoints = np.loadtxt(flowlinepath+'/newpoints.dat')
